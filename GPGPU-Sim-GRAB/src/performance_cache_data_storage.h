/*
 * cache_performance_data_storage.h
 *
 *  Created on: Aug 20, 2014
 *      Author: adrianh
 */

#ifndef PERFORMANCE_CACHE_DATA_STORAGE_H_INCLUDED
#define PERFORMANCE_CACHE_DATA_STORAGE_H_INCLUDED

#include <vector>
#include "cuda-sim/memory.h"
#include "gpgpu-sim/gpu-cache.h"
#include "abstract_hardware_model.h"
#include <iostream>
#include <tuple>
#include <utility>
#include <string>

#define CACHE_BEBUG 1


enum cm_access_type {CM_HIT, CM_MISS, CM_CAPACITY_MISS, CM_NOT_ACCESSED, CM_UNRECOGNIZED};
enum cm_problem {PRB_NONE,	//for Hit-Miss and Hit-Hit
				PRB_M_M ,	//if root cause then too much data or replacement policy, includes PRB_CAPM_M
				PRB_M_H,	//poor cache management
				PRB_CAPM_H};//too many threads

const std::string cm_problem_string[] = {"NONE", "Fault_MM", "Fault_MH", "Fault_M*H"};

class CacheHistoryBlock{
private:
	unsigned sid;
	address_type pc;
	new_addr_type address;
	active_mask_t access_thread_mask;
	unsigned int warp_id;
	cm_access_type normal_cache_access; //for normal scenario
	std::vector<cm_access_type> ideal_cache_access; //for ideal scenario
	new_addr_type evicted_mem_address;	//ejects entire cache block
	long rootIndex;
	//std::map<cm_problem, std::vector<CacheHistoryBlock*>> *problemsGenerated;
	cm_problem problem;

public:
	CacheHistoryBlock(unsigned shader_id,
			address_type programCounter,
			new_addr_type blockAddress,
			active_mask_t accessThreadsMaks,
			unsigned int warpId,
			cm_access_type normalAccess,
			std::vector<cm_access_type> idealAccess,
			new_addr_type evictedBlockAddress){
		sid = shader_id;
		pc = programCounter;
		address = blockAddress;
		access_thread_mask =accessThreadsMaks;
		warp_id = warpId;
		normal_cache_access = normalAccess;
		ideal_cache_access = idealAccess;
		evicted_mem_address = evictedBlockAddress;
		problem = PRB_NONE;
		rootIndex = -1;
	};

	friend std::ostream& operator<<(std::ostream& os, const CacheHistoryBlock& instance);
	void addNewGeneratedProblem(CacheHistoryBlock *block);
	bool hasProblem();
	void computeProblem();
	new_addr_type getAddress();
	unsigned getShaderID();
	new_addr_type getEvictedAddress();
	address_type getPC();
	cm_problem getProblem();
	void setRootIndex(long index);
	long getRootIndex();

};


class CachePerformanceHistory {
public:
	void addBlock(CacheHistoryBlock* block);
	void computeRootCause(CacheHistoryBlock* block);
	long getLastAddressIndex(unsigned sid, new_addr_type address);
	static CachePerformanceHistory* getInstance();
	void printList();
	~CachePerformanceHistory(){
		instanceFlag = false;
	}
	void clear();

	cm_access_type transformForNomalCache(cache_request_status status, unsigned shader_id, new_addr_type address, l1_cache *cache);
	cm_access_type transformForIdealCache(cache_request_status status);
	l1_cache* getIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id);
	void addIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id, l1_cache* cache);
	void fillIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id, const mem_fetch *mf, unsigned time);
	void flushIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id);

private:
	static bool instanceFlag;
	static CachePerformanceHistory *singleton;

	std::vector<CacheHistoryBlock*> history; //for saving at each step (instruction)
	std::map<unsigned, std::map<new_addr_type, long>> lastEvictedMemoryIndex;
	std::map<std::tuple<unsigned, unsigned, unsigned>, l1_cache*> idealCaches;
	std::map<cm_problem,
				std::map<address_type,
							std::map<address_type, unsigned>>> problemCounter;
};

#endif /* PERFORMANCE_CACHE_DATA_STORAGE_H_INCLUDED */
