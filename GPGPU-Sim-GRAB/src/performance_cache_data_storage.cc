/*
 * cache_performance_data_storage.cc
 *
 *  Created on: Aug 20, 2014
 *      Author: adrianh
 */


#include "performance_cache_data_storage.h"
#include <iostream>


std::ostream& operator<<(std::ostream& os, const CacheHistoryBlock& instance) {
	os << ""
			<< " Shader: " << instance.sid
			<< " PC: " << instance.pc
			<< " Address: " << instance.address
			<< " Evicted address: " << instance.evicted_mem_address
			<< " Warp ID: " << instance.warp_id
			<< " Active mask: " << instance.access_thread_mask
			<< " Root: " << instance.rootIndex
			<< " \n "
			<< " Normal access: " << instance.normal_cache_access
			<< " Ideal access: ";

	for (auto c : instance.ideal_cache_access)
		os << c << "";


	return os;
}

bool CacheHistoryBlock::hasProblem(){
	return (problem != PRB_NONE)?true:false;
}
void CacheHistoryBlock::computeProblem(){

	long index = CachePerformanceHistory::getInstance()->getLastAddressIndex(sid, address);
	//if no root cause, then no problem
	if ((index == -1) || (normal_cache_access == CM_HIT)){
		problem = PRB_NONE;
		return;
	}

	cm_access_type found = CM_MISS;
	for (cm_access_type i : ideal_cache_access){
		if (i == CM_HIT){
			found = CM_HIT;
		}

	}

	if (((normal_cache_access == CM_CAPACITY_MISS) || (normal_cache_access == CM_MISS)) && (found == CM_MISS)){
		problem = PRB_M_M;
	}

	if ((normal_cache_access == CM_MISS) && (found == CM_HIT)){
		problem = PRB_M_H;
	}

	if ((normal_cache_access == CM_CAPACITY_MISS) && (found == CM_HIT)){
		problem = PRB_CAPM_H;
	}


}

unsigned CacheHistoryBlock::getShaderID(){
	return sid;
}

new_addr_type CacheHistoryBlock::getAddress(){
	return address;
}

new_addr_type CacheHistoryBlock::getEvictedAddress(){
	return evicted_mem_address;
}

address_type CacheHistoryBlock::getPC(){
	return pc;
}

cm_problem CacheHistoryBlock::getProblem(){
	return problem;
}


void CacheHistoryBlock::setRootIndex(long index){
	rootIndex = index;
}

long CacheHistoryBlock::getRootIndex(){
	return rootIndex;
}

bool CachePerformanceHistory::instanceFlag = false;
CachePerformanceHistory* CachePerformanceHistory::singleton = NULL;
CachePerformanceHistory* CachePerformanceHistory::getInstance(){
	if (instanceFlag == false){
		instanceFlag =true;
		singleton = new CachePerformanceHistory();
	}
	return singleton;

}

void CachePerformanceHistory::addBlock(CacheHistoryBlock *block){
	history.push_back(block);
	if (block->getEvictedAddress() != 0)
		lastEvictedMemoryIndex[block->getShaderID()][block->getEvictedAddress()] = history.size() - 1;
}

void CachePerformanceHistory::clear(){
	for (int i =0; i< history.size();i++)
	{
		delete (history[i]);
	}
	history.clear();
	lastEvictedMemoryIndex.clear();

	for(std::map<std::tuple<unsigned, unsigned, unsigned>, l1_cache*>::iterator itr = idealCaches.begin(); itr != idealCaches.end(); itr++)
	{
		delete itr->second;
	}
	idealCaches.clear();
	problemCounter.clear();
}


void CachePerformanceHistory::printList(){
	/*	for (const auto element : history){
		std::cout << *element << "\n";
	}*/

	std::cout << "\n\n ------------------------------------------------------------------- \n\n";
	std::cout << "				Problems found \n\n";

	for (auto type : problemCounter){
		long total = 0;
		std::cout << "Type : " << type.first << " - " << cm_problem_string[type.first] <<"\n";
		for (auto lines : type.second){
			std::cout << "	PC : " << lines.first << "\n";
			long sum = 0;
			std::cout << "		(PC, problems) generated : \n";
			std::cout << "		";
			for (auto element : lines.second){
				std::cout << " (" << element.first << "," << element.second << ") , ";
				sum += element.second;
			}
			std::cout << " \n	Generates a total of " << sum << " problems\n";
			total += sum;
		}

		std::cout << " ------------ \n";
		std::cout << "Total problems of type " << type.first << " : " << total << "\n";
		std::cout << " ------------ \n\n";

	}


	std::cout << "\n\n ------------------------------------------------------------------- \n\n";

}

cm_access_type CachePerformanceHistory::transformForNomalCache(cache_request_status status, unsigned shader_id, new_addr_type address, l1_cache *cache){

	if (status == HIT){
		return CM_HIT;
	}else
		if ((status == MISS) || (status == HIT_RESERVED)){
			if (cache->isFull()){
				cache->logAccess(address);
				return CM_CAPACITY_MISS;
			}
			else{
				cache->logAccess(address);
				return CM_MISS;
			}
		}

	return CM_UNRECOGNIZED;

}

cm_access_type CachePerformanceHistory::transformForIdealCache(cache_request_status status){
	if (status == HIT){
		return CM_HIT;
	}else
		if (status == MISS){
			return CM_MISS;
		}
	return CM_UNRECOGNIZED;
}


l1_cache* CachePerformanceHistory::getIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id){

	std::tuple<unsigned, unsigned, unsigned> tup = std::make_tuple(shader_id, warp_id, thread_id);
	if (idealCaches.find(tup) == idealCaches.end())
		return nullptr;
	else
		return idealCaches.at(tup);
}

void CachePerformanceHistory::addIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id, l1_cache* cache){
	std::tuple<unsigned, unsigned, unsigned> tup = std::make_tuple(shader_id, warp_id, thread_id);
	idealCaches[tup] = cache;
}

void CachePerformanceHistory::flushIdealCache(unsigned shader_id, unsigned warp_id, unsigned thread_id){
	std::tuple<unsigned, unsigned, unsigned> tup = std::make_tuple(shader_id, warp_id, thread_id);
	if (idealCaches.find(tup) != idealCaches.end()){
		idealCaches[tup]->flush();
	}
}


long CachePerformanceHistory::getLastAddressIndex(unsigned sid, new_addr_type address){

	if (lastEvictedMemoryIndex.find(sid) != lastEvictedMemoryIndex.end()){
		if (lastEvictedMemoryIndex[sid].find(address) != lastEvictedMemoryIndex[sid].end()){
			long index = lastEvictedMemoryIndex[sid][address];
			return index;
		}
	}
	return -1;
}


void CachePerformanceHistory::computeRootCause(CacheHistoryBlock* block){

	//std::cout << "finding root cause \n";
	block->computeProblem();
	if (block->hasProblem()){
		long index = lastEvictedMemoryIndex[block->getShaderID()][block->getAddress()];
		CacheHistoryBlock* blk = history[index];
		long rootIndex = blk->getRootIndex();

		if (rootIndex == -1){
			block->setRootIndex(index);
		}else{
			block->setRootIndex(rootIndex);
		}
		blk = history[block->getRootIndex()];
		unsigned rootPC = blk->getPC();
		cm_problem targetProblem = block->getProblem();
		unsigned targetPC = block->getPC();

		//	std::map<address_type,std::vector<std::pair<address_type, unsigned>>> *pcMap = &problemCounter[problem];
		//	std::vector<std::pair<address_type, unsigned>> *list = &pcMap[pc];


		problemCounter[targetProblem][rootPC][targetPC]++;
	}
}


