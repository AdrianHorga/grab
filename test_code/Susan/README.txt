Compile any of the 3 versions with nvcc:

nvcc -o <executable_name> Susan_<version>.cu

Run the example using "-p" and "-3" as input arguments:

./<executable_name> lena.pgm <output_image_name>.pgm -p -3 
