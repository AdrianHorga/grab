#undef __FUNC__
#define __FUNC__ periodic_boundary

__constant__ double awaals = ((double) 9) / ((double) 8);
__constant__ int nvelocities, kn,kux, kuy, kpid, kpwaals, kdpress, kforcex, kforcey, kfeq;

__constant__ int nodes_x, nodes_y, nodes, lnodes, lnodes_x, lnodes_y, nvar, nw, nbasevelocities;
__constant__ int niter, niter_cycle, ncycles, last_iter;

__constant__ double zero, one, two, three, four, five, eight;
__constant__ double delta_time, dtimetemp;
__constant__ double cfl;

__constant__ double delta_space, two_delta_space, delta_space_three_three;

__constant__ double chic2, twochic2, twochi2c4;

__constant__ double tau, kappa, tfluid, tcritical;

__device__ int getGpos(){
	return blockDim.x * blockIdx.x + threadIdx.x;
}

__device__ int getTiy(int gpos){
	return gpos / nodes_x;
}

__device__ int getTix(int gpos){
	return gpos % nodes_x;
}

__device__ int getLpos(int gpos){
	return (getTiy(gpos) + nw) * lnodes_x + getTix(gpos) + nw;
}


__global__ void periodic_boundary(double *dgf_one, double *dgf_two, double *cspeed_x,double *cspeed_y)
{

	long gpos, lpos;
	long laux;
	double aux1,aux2, aux3;

	gpos = getGpos();


	while (gpos < nodes)
	{
		lpos = (getTiy(gpos) + nw) * lnodes_x + getTix(gpos) + nw;


		aux1 = dgf_one[kn*nodes+gpos];
		aux2 = aux1 * tfluid;
		aux3 = aux2 * three / (three - aux1) - awaals * tcritical * aux1 * aux1;

		dgf_one[kpid*nodes+gpos] = aux2;
		dgf_one[kpwaals*nodes+gpos] = aux3;
		dgf_one[kdpress*nodes+gpos] = aux2 - aux3;


		aux1 = zero;
		aux2 = zero;
		aux3 = zero;

		for(int k=0; k < nvelocities; k++)
		{

			aux1 += dgf_one[k*nodes+gpos];
			aux2 += dgf_one[k*nodes+gpos] * cspeed_x[k];
			aux3 += dgf_one[k*nodes+gpos] * cspeed_y[k];
		}

		dgf_one[kn*nodes+gpos] = aux1;
		dgf_one[kux*nodes+gpos] = aux2 / aux1;
		dgf_one[kuy*nodes+gpos] = aux3 / aux1;



		for(int k=0; k < kfeq; k++)
		{
			laux = k*lnodes+lpos;
			/* Copy dgf to dlf */
			dgf_two[laux] = dgf_one[k*nodes+gpos];

			aux1 = dgf_two[laux];
			/* Build dlf with periodic conditions */
			if(getTix(gpos) < nw)
			{
				//least
				dgf_two[laux+nodes_x] = aux1;
			}
			if(getTix(gpos) > nodes_x - nw -1 /*nxmnwm1*/)
			{
				//lwest
				dgf_two[laux-nodes_x] = aux1;
			}
			if(getTiy(gpos) < nw)
			{
				//lnorth
				dgf_two[laux + nodes_y * lnodes_x] = aux1;

				if(getTix(gpos) < nw)
				{
					//lnortheast
					dgf_two[laux + nodes_y * lnodes_x + nodes_x] = aux1;
				}
				if(getTix(gpos) > nodes_x - nw -1 /*nxmnwm1*/)
				{
					//lnorthwest
					dgf_two[laux + nodes_y * lnodes_x - nodes_x] = aux1;
				}
			}
			if(getTiy(gpos) > nodes_y - nw - 1 /*nymnwm1*/)
			{
				//lsouth
				dgf_two[laux - nodes_y * lnodes_x] = aux1;

				if(getTix(gpos) < nw)
				{
					//lsoutheast
					dgf_two[laux - nodes_y * lnodes_x + nodes_x] = aux1;
				}
				if(getTix(gpos) > nodes_x - nw - 1 /*nxmnwm1*/)
				{
					//lsouthwest
					dgf_two[laux - nodes_y * lnodes_x - nodes_x] = aux1;
				}
			}
		}

		gpos += gridDim.x * blockDim.x;
	}
}


#undef __FUNC__
#define __FUNC__ lb_time_step


__constant__ double waxis  = ((double) 1) / ((double) 9);
__constant__ double wdiag  = ((double) 1) / ((double) 36);

__global__ void lb_time_step(double *dgf_one, double *dgf_two, double *cspeed_x, double *cspeed_y,
		double *cweight)
{

	long gpos, lpos;

	long laux;

	double ntau;
	double nn, ux, uy, edotu;
	double dxpress, dypress;
	double dxlapn, dylapn;

	double aux1,aux2,aux3,aux4;

	gpos = getGpos();

	while (gpos < nodes)
	{
		lpos = (getTiy(gpos) + nw) * lnodes_x + getTix(gpos) + nw;


		/*  Computing the force terms  */

		laux = (kdpress * lnodes) + lpos;

		/*  2 x 9 point stencil */

		aux1 = dgf_two[laux + 1];
		aux1 -= dgf_two[laux - 1];
		aux1 *= waxis;


		aux2 = dgf_two[laux + lnodes_x + 1];
		aux2 -= dgf_two[laux + lnodes_x - 1];
		aux2 -= dgf_two[laux - lnodes_x - 1];
		aux2 += dgf_two[laux - lnodes_x + 1];
		aux2 *= wdiag;
		aux3 = (dgf_two[laux + 2] - dgf_two[laux - 2]) * waxis;
		aux4 = (dgf_two[laux + 2 * (lnodes_x + 1)] - dgf_two[laux + 2 * (lnodes_x -1)] -
				dgf_two[laux - 2 * (lnodes_x + 1)] + dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag;

		dxpress = ((aux1 + aux2)* eight - (aux3 + aux4))/two_delta_space;


		aux1 = (dgf_two[laux + lnodes_x] - dgf_two[laux - lnodes_x]) * waxis;
		aux2 = (dgf_two[laux + lnodes_x + 1] + dgf_two[laux + lnodes_x - 1] -
				dgf_two[laux - lnodes_x - 1] - dgf_two[laux - lnodes_x + 1]) * wdiag;
		aux3 = (dgf_two[laux + 2 * lnodes_x] - dgf_two[laux - 2 * lnodes_x]) * waxis;
		aux4 = (dgf_two[laux + 2 * (lnodes_x + 1)] + dgf_two[laux + 2 * (lnodes_x -1)] -
				dgf_two[laux - 2 * (lnodes_x + 1)] - dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag;

		dypress = ((aux1 + aux2)* eight - (aux3 + aux4))/two_delta_space;
//		if (gpos==0)
//			printf("dxpress=%lf dypress=%lf \n", dxpress, dypress);

		laux = (kn * lnodes) + lpos;


		dxlapn = ((dgf_two[laux + 2] - dgf_two[laux - 2]) * waxis
				+ (dgf_two[laux + 2 * (lnodes_x + 1)] - dgf_two[laux + 2 * (lnodes_x - 1)] -
						dgf_two[laux - 2 * (lnodes_x + 1)] + dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag
						- ((dgf_two[laux + 1] - dgf_two[laux - 1]) * waxis +
								(dgf_two[laux + lnodes_x + 1] - dgf_two[laux + lnodes_x - 1] -
										dgf_two[laux - lnodes_x - 1] + dgf_two[laux - lnodes_x + 1]) * wdiag) *
										two) / delta_space_three_three;


		dylapn = ((dgf_two[laux + 2 * lnodes_x] - dgf_two[laux - 2 * lnodes_x]) * waxis
				+ (dgf_two[laux + 2 * (lnodes_x + 1)] + dgf_two[laux + 2 * (lnodes_x - 1)] -
						dgf_two[laux - 2 * (lnodes_x + 1)] - dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag -
						((dgf_two[laux + lnodes_x] - dgf_two[laux - lnodes_x]) * waxis +
								(dgf_two[laux + lnodes_x + 1] + dgf_two[laux + lnodes_x - 1] -
										dgf_two[laux - lnodes_x - 1] - dgf_two[laux - lnodes_x + 1]) * wdiag) *
										two) / delta_space_three_three;



		dgf_two[kforcex*lnodes+lpos] = dxpress / dgf_two[kn*lnodes+lpos] + kappa * dxlapn;
		dgf_two[kforcey*lnodes+lpos] = dypress / dgf_two[kn*lnodes+lpos] + kappa * dylapn;


		/* Relaxation + Equilibrium distribution function */

		nn = dgf_two[kn*lnodes+lpos];
		ux = dgf_two[kux*lnodes+lpos];
		uy = dgf_two[kuy*lnodes+lpos];


		//nn = zero;
		//ntau = tau / nn;
		ntau = tau;

		dgf_two[kfeq*lnodes+lpos] = one - (ux * ux + uy * uy) / twochic2;

		for(int k=1; k < nvelocities; k++)
		{
			edotu = cspeed_x[k] * ux + cspeed_y[k] * uy;

			dgf_two[(k+kfeq)*lnodes+lpos] = (dgf_two[kfeq*lnodes+lpos] + edotu / chic2 + edotu * edotu / twochi2c4) *
					nn  * cweight[k];

		}

		dgf_two[kfeq*lnodes+lpos] *= (nn  * cweight[0]);

		//dtimetau = delta_time / ntau;
		/*
		dgf_one[0*nodes+gpos] = dgf_two[0*lnodes+lpos];
		dgf_one[1*nodes+gpos] = dgf_two[1*lnodes+lpos] - cfl * (dgf_two[1*lnodes+lpos] - dgf_two[upos1]);
		dgf_one[2*nodes+gpos] = dgf_two[2*lnodes+lpos] - cfl * (dgf_two[2*lnodes+lpos] - dgf_two[upos2]);
		dgf_one[3*nodes+gpos] = dgf_two[3*lnodes+lpos] - cfl * (dgf_two[3*lnodes+lpos] - dgf_two[upos3]);
		dgf_one[4*nodes+gpos] = dgf_two[4*lnodes+lpos] - cfl * (dgf_two[4*lnodes+lpos] - dgf_two[upos4]);
		dgf_one[5*nodes+gpos] = dgf_two[5*lnodes+lpos] - cfl * (dgf_two[5*lnodes+lpos] - dgf_two[upos5]);
		dgf_one[6*nodes+gpos] = dgf_two[6*lnodes+lpos] - cfl * (dgf_two[6*lnodes+lpos] - dgf_two[upos6]);
		dgf_one[7*nodes+gpos] = dgf_two[7*lnodes+lpos] - cfl * (dgf_two[7*lnodes+lpos] - dgf_two[upos7]);
		dgf_one[8*nodes+gpos] = dgf_two[8*lnodes+lpos] - cfl * (dgf_two[8*lnodes+lpos] - dgf_two[upos8]);
		 */

		dgf_one[0*nodes+gpos] = dgf_two[0*lnodes+lpos];
		dgf_one[1*nodes+gpos] = dgf_two[1*lnodes+lpos] - cfl * (dgf_two[1*lnodes+lpos] - dgf_two[1 * lnodes + lpos - 1]);
		dgf_one[2*nodes+gpos] = dgf_two[2*lnodes+lpos] - cfl * (dgf_two[2*lnodes+lpos] - dgf_two[2 * lnodes + lpos - lnodes_x]);
		dgf_one[3*nodes+gpos] = dgf_two[3*lnodes+lpos] - cfl * (dgf_two[3*lnodes+lpos] - dgf_two[3 * lnodes + lpos + 1]);
		dgf_one[4*nodes+gpos] = dgf_two[4*lnodes+lpos] - cfl * (dgf_two[4*lnodes+lpos] - dgf_two[4 * lnodes + lpos + lnodes_x]);
		dgf_one[5*nodes+gpos] = dgf_two[5*lnodes+lpos] - cfl * (dgf_two[5*lnodes+lpos] - dgf_two[5 * lnodes + lpos - lnodes_x - 1]);
		dgf_one[6*nodes+gpos] = dgf_two[6*lnodes+lpos] - cfl * (dgf_two[6*lnodes+lpos] - dgf_two[6 * lnodes + lpos - lnodes_x + 1]);
		dgf_one[7*nodes+gpos] = dgf_two[7*lnodes+lpos] - cfl * (dgf_two[7*lnodes+lpos] - dgf_two[7 * lnodes + lpos + lnodes_x + 1]);
		dgf_one[8*nodes+gpos] = dgf_two[8*lnodes+lpos] - cfl * (dgf_two[8*lnodes+lpos] - dgf_two[8 * lnodes + lpos + lnodes_x - 1]);

		for(int k=0; k < nvelocities; k++)
		{

			//csux = cspeed_x[k] - ux;
			//csuy = cspeed_y[k] - uy;

			dgf_one[k*nodes+gpos] -= ((dgf_two[k*lnodes+lpos] - dgf_two[(k+kfeq)*lnodes+lpos]) * (delta_time / ntau /*dtimetau*/)
					- dgf_two[(k+kfeq)*lnodes+lpos] * dtimetemp * (dgf_two[kforcex*lnodes+lpos] * (cspeed_x[k] - ux/*csux*/)
							+ dgf_two[kforcey*lnodes+lpos] * (cspeed_y[k] - uy/*csuy*/)));
		}

		gpos += gridDim.x * blockDim.x;
	}

}

