#ifndef lbmInput
#define lbmInput

#include <string.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#define SCANF_NO_INPUT -1
#define SCANF_NOTHING_READ 0
#define SUCCESS 1
#define FAILURE -1


typedef struct lbmInputParameters{
	char* name;
	char* value;
}InputParameters;

 InputParameters params[25];
 int paramsCount = 0;

 template <class pointerType>
  void CHK_NULL_RETURN(pointerType ptr){
 	if (ptr == NULL){
 		printf("Error, ptr is NULL\n");
 		exit(EXIT_FAILURE);
 	}
 }

 void readInputParameters(char* filename)
{
	FILE *f;
	int i = 0;
	char dummy[30] = "";

	CHK_NULL_RETURN(f = fopen(filename,"r"));


	while(!feof(f))
	{
		if (fscanf(f,"%s",dummy) != SCANF_NO_INPUT)
		{
		params[i].name = strdup(dummy);

		if (fscanf(f,"%s",dummy) != SCANF_NOTHING_READ)
		{
			params[i].value = strdup(dummy);
			paramsCount++;
		}
		else
		{
			exit(FAILURE);
		}
			
		}

		i++;
	}
}

 int GetInt(const char* name,int *value)
{
	int i = 0;
	for (i=0;i<paramsCount;i++)
	{
		if (strcmp(params[i].name,name) == 0)
		{
			
			*value = strtol(params[i].value, NULL, 10);
			return SUCCESS;
		}
	}
	return FAILURE;
}


 double GetDouble(const char* name,double *value)
{
	int i = 0;
	for (i=0;i<paramsCount;i++)
	{
		if (strcmp(params[i].name,name) == 0)
		{
			
			*value = strtod(params[i].value, NULL);
			return 1;
		}
	}
	return -1;

}

 double fp_uniform(double a, double b)
{
	double t;
	unsigned char *p = (unsigned char *)&t;
	size_t i, n = 0;
	do
	{
		for(i = 0; i < sizeof (double); i++)
			{
				p[i] = rand() / (RAND_MAX + 1.0) * (UCHAR_MAX + 1.0);
			}
		n++;
	}
	while(t == 0 /*|| isnan(t)*/ || t < a || t > b);
	return t;
}

/* double drand48()
{
	return rand() / (RAND_MAX + 1.);
}*/

 double randomUniformInterval(double a, double b)
{
	return drand48() * (b-a) + a;
}


#endif
