/******************************************************************************
 *                                                                             *
 *   van der waals -- isothermal version -- gpu-cuda (2013)                    *
 *                                                                             *
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <cuda.h>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include "nw2cuda.h"
#include "nw2cudainout.h"
#include "nw2cudainit.h"

#undef __FUNC__
#define __FUNC__ periodic_boundary

__constant__ double awaals = ((double) 9) / ((double) 8);
__constant__ int nvelocities, kn,kux, kuy, kpid, kpwaals, kdpress, kforcex, kforcey, kfeq;

__constant__ int nodes_x, nodes_y, nodes, lnodes, lnodes_x, lnodes_y, nvar, nw, nbasevelocities;
__constant__ int niter, niter_cycle, ncycles, last_iter;

__constant__ double zero, one, two, three, four, five, eight;
__constant__ double delta_time, dtimetemp;
__constant__ double cfl;

__constant__ double delta_space, two_delta_space, delta_space_three_three;

__constant__ double chic2, twochic2, twochi2c4;

__constant__ double tau, kappa, tfluid, tcritical;


__global__ void periodic_boundary(double *dgf_one, double *dgf_two, double *cspeed_x,double *cspeed_y)
{

	int tix, tiy;
	long gpos, lpos;
	long laux;
	
	double aux1,aux2,aux3,aux4;

	gpos = blockDim.x * blockIdx.x + threadIdx.x;


	while (gpos < nodes)
	{
		tiy = gpos / nodes_x;
		tix = gpos % nodes_x;
		lpos = (tiy + nw) * lnodes_x + tix + nw;


		dgf_one[kpid*nodes+gpos] = dgf_one[kn*nodes+gpos] * tfluid;

		dgf_one[kpwaals*nodes+gpos] = dgf_one[kpid*nodes+gpos] * three / (three - dgf_one[kn*nodes+gpos]) -
					              awaals * tcritical * dgf_one[kn*nodes+gpos] * dgf_one[kn*nodes+gpos];

		dgf_one[kdpress*nodes+gpos] = dgf_one[kpid*nodes+gpos] - dgf_one[kpwaals*nodes+gpos];

		aux2 = zero;
		aux3 = zero;
		aux4 = zero;

		for(int k=0; k < nvelocities; k++)
		{
			aux1 = dgf_one[k*nodes+gpos];
			
			aux2 += aux1;
			aux3 += aux1 * cspeed_x[k];
			aux4 += aux1 * cspeed_y[k];
		}
		
		
		dgf_one[kn*nodes+gpos] = aux2;
		dgf_one[kux*nodes+gpos] = aux3 / aux2;
		dgf_one[kuy*nodes+gpos] = aux4 / aux2;




		for(int k=0; k < kfeq; k++)
		{
			laux = k*lnodes+lpos;
			/* Copy dgf to dlf */
			dgf_two[laux] = dgf_one[k*nodes+gpos];

			/* Build dlf with periodic conditions */
			if(tix < nw)
			{
				//least
				dgf_two[laux+nodes_x] = dgf_two[laux];
			}
			if(tix > nodes_x - nw -1 /*nxmnwm1*/)
			{
				//lwest
				dgf_two[laux-nodes_x] = dgf_two[laux];
			}
			if(tiy < nw)
			{
				//lnorth
				dgf_two[laux + nodes_y * lnodes_x] = dgf_two[laux];

			  if(tix < nw)
				{
					//lnortheast
					dgf_two[laux + nodes_y * lnodes_x + nodes_x] = dgf_two[laux];
				}
				if(tix > nodes_x - nw -1 /*nxmnwm1*/)
				{
					//lnorthwest
					dgf_two[laux + nodes_y * lnodes_x - nodes_x] = dgf_two[laux];
				}
			}
			 if(tiy > nodes_y - nw - 1 /*nymnwm1*/)
			 {
				 //lsouth
				dgf_two[laux - nodes_y * lnodes_x] = dgf_two[laux];

				 if(tix < nw)
				 {
					 //lsoutheast
					dgf_two[laux - nodes_y * lnodes_x + nodes_x] = dgf_two[laux];
				 }
				 if(tix > nodes_x - nw - 1 /*nxmnwm1*/)
				 {
					 //lsouthwest
					dgf_two[laux - nodes_y * lnodes_x - nodes_x] = dgf_two[laux];
				 }
			 }
		}

		gpos += gridDim.x * blockDim.x;
	}
}


#undef __FUNC__
#define __FUNC__ lb_time_step


__constant__ double waxis  = ((double) 1) / ((double) 9);
__constant__ double wdiag  = ((double) 1) / ((double) 36);

__global__ void lb_time_step(double *dgf_one, double *dgf_two, double *cspeed_x, double *cspeed_y,
															double *cweight)
{

	//int klnodes;
	int tix, tiy;

	long gpos, lpos;

	long laux;

	double ntau;
	//double dtimetau, csux, csuy;
	double nn, ux, uy, edotu;
	double dxpress, dypress;
	double dxlapn, dylapn;

	gpos = blockDim.x * blockIdx.x + threadIdx.x;

	while (gpos < nodes)
	{

		tiy = gpos / nodes_x;
		tix = gpos % nodes_x;
		lpos = (tiy + nw) * lnodes_x + tix + nw;


	  /*  Computing the force terms  */
		//klnodes = (kdpress * lnodes);
		laux = (kdpress * lnodes) + lpos;

/*		slpos0  = zero;
		slpos1  = ((tiy + nw) * lnodes_x) + tix + nw + 1;		laux + 1
		slpos2  = ((tiy + nw + 1) * lnodes_x) + tix + nw;		laux + lnodes_x
		slpos3  = ((tiy + nw) * lnodes_x) + tix + nw - 1;		laux - 1
		slpos4  = ((tiy + nw - 1) * lnodes_x) + tix + nw;		laux - lnodes_x
		slpos5  = ((tiy + nw + 1) * lnodes_x) + tix + nw + 1;		laux + lnodes_x + 1
		slpos6  = ((tiy + nw + 1) * lnodes_x) + tix + nw - 1;		laux + lnodes_x - 1
		slpos7  = ((tiy + nw - 1) * lnodes_x) + tix + nw - 1;		laux - lnodes_x - 1
		slpos8  = ((tiy + nw - 1) * lnodes_x) + tix + nw + 1;		laux - lnodes_x + 1
		slpos9  = ((tiy + nw) * lnodes_x) + tix + nw + 2;		laux + 2
		slpos10 = ((tiy + nw + 2) * lnodes_x) + tix + nw;		laux + 2 * lnodes_x
		slpos11 = ((tiy + nw) * lnodes_x) + tix + nw - 2;		laux - 2
		slpos12 = ((tiy + nw - 2) * lnodes_x) + tix + nw;		laux - 2 * lnodes_x
		slpos13 = ((tiy + nw + 2) * lnodes_x) + tix + nw + 2;		laux + 2 * (lnodes_x + 1)
		slpos14 = ((tiy + nw + 2) * lnodes_x) + tix + nw - 2;		laux + 2 * (lnodes_x - 1)
		slpos15 = ((tiy + nw - 2) * lnodes_x) + tix + nw - 2;		laux - 2 * (lnodes_x + 1)
		slpos16 = ((tiy + nw - 2) * lnodes_x) + tix + nw + 2;		laux - 2 * (lnodes_x - 1)
*/

		/*  2 x 9 point stencil */

		dxpress = (((dgf_two[laux + 1] - dgf_two[laux - 1]) * waxis
		 						+ (dgf_two[laux + lnodes_x + 1] - dgf_two[laux + lnodes_x - 1] -
								dgf_two[laux - lnodes_x - 1] + dgf_two[laux - lnodes_x + 1]) * wdiag)
						    * eight -
		((dgf_two[laux + 2] - dgf_two[laux - 2]) * waxis +
		(dgf_two[laux + 2 * (lnodes_x + 1)] - dgf_two[laux + 2 * (lnodes_x -1)] -
		dgf_two[laux - 2 * (lnodes_x + 1)] + dgf_two[laux - 2 * (lnodes_x - 1)]) *
		wdiag)) / two_delta_space;

		dypress = (((dgf_two[laux + lnodes_x] - dgf_two[laux - lnodes_x]) * waxis
								+ (dgf_two[laux + lnodes_x + 1] + dgf_two[laux + lnodes_x - 1] -
								dgf_two[laux - lnodes_x - 1] - dgf_two[laux - lnodes_x + 1]) * wdiag)
								* eight -
		((dgf_two[laux + 2 * lnodes_x] - dgf_two[laux - 2 * lnodes_x]) * waxis +
		(dgf_two[laux + 2 * (lnodes_x + 1)] + dgf_two[laux + 2 * (lnodes_x -1)] -
		dgf_two[laux - 2 * (lnodes_x + 1)] - dgf_two[laux - 2 * (lnodes_x - 1)]) *
		wdiag)) / two_delta_space;

//		if (gpos == 0)
	//		printf("dx=%lf dy=%lf\n", dxpress, dypress);


		//klnodes = (kn * lnodes);
		laux = (kn * lnodes) + lpos;


		dxlapn = ((dgf_two[laux + 2] - dgf_two[laux - 2]) * waxis
							+ (dgf_two[laux + 2 * (lnodes_x + 1)] - dgf_two[laux + 2 * (lnodes_x - 1)] -
							dgf_two[laux - 2 * (lnodes_x + 1)] + dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag
							- ((dgf_two[laux + 1] - dgf_two[laux - 1]) * waxis +
							(dgf_two[laux + lnodes_x + 1] - dgf_two[laux + lnodes_x - 1] -
							dgf_two[laux - lnodes_x - 1] + dgf_two[laux - lnodes_x + 1]) * wdiag) *
							two) / delta_space_three_three;


	dylapn = ((dgf_two[laux + 2 * lnodes_x] - dgf_two[laux - 2 * lnodes_x]) * waxis
							+ (dgf_two[laux + 2 * (lnodes_x + 1)] + dgf_two[laux + 2 * (lnodes_x - 1)] -
							dgf_two[laux - 2 * (lnodes_x + 1)] - dgf_two[laux - 2 * (lnodes_x - 1)]) * wdiag -
							((dgf_two[laux + lnodes_x] - dgf_two[laux - lnodes_x]) * waxis +
							(dgf_two[laux + lnodes_x + 1] + dgf_two[laux + lnodes_x - 1] -
							dgf_two[laux - lnodes_x - 1] - dgf_two[laux - lnodes_x + 1]) * wdiag) *
							two) / delta_space_three_three;



	  dgf_two[kforcex*lnodes+lpos] = dxpress / dgf_two[kn*lnodes+lpos] + kappa * dxlapn;
	  dgf_two[kforcey*lnodes+lpos] = dypress / dgf_two[kn*lnodes+lpos] + kappa * dylapn;
	//dgf_two[kforcex*lnodes+lpos] = zero;
	  //dgf_two[kforcey*lnodes+lpos] = zero;


/*		upos0 = 0;
		upos1 = 1 * lnodes + ((tiy + nw) * lnodes_x) + tix + nw - 1;		1 * lnodes + lpos - 1
		upos2 = 2 * lnodes + ((tiy + nw - 1) * lnodes_x) + tix + nw;		2 * lnodes + lpos - lnodes_x
		upos3 = 3 * lnodes + ((tiy + nw) * lnodes_x) + tix + nw + 1;		3 * lnodes + lpos + 1
		upos4 = 4 * lnodes + ((tiy + nw + 1) * lnodes_x) + tix + nw ;		4 * lnodes + lpos + lnodes_x
		upos5 = 5 * lnodes + ((tiy + nw - 1) * lnodes_x) + tix + nw - 1;	5 * lnodes + lpos - lnodes_x - 1
		upos6 = 6 * lnodes + ((tiy + nw - 1) * lnodes_x) + tix + nw + 1;	6 * lnodes + lpos - lnodes_x + 1
		upos7 = 7 * lnodes + ((tiy + nw + 1) * lnodes_x) + tix + nw + 1;	7 * lnodes + lpos + lnodes_x + 1
	  	upos8 = 8 * lnodes + ((tiy + nw + 1) * lnodes_x) + tix + nw - 1;	8 * lnodes + lpos + lnodes_x - 1
*/

		/* Relaxation + Equilibrium distribution function */

		nn = dgf_two[kn*lnodes+lpos];
		ux = dgf_two[kux*lnodes+lpos];
		uy = dgf_two[kuy*lnodes+lpos];


		//nn = zero;
		//ntau = tau / nn;
		ntau = tau;

		dgf_two[kfeq*lnodes+lpos] = one - (ux * ux + uy * uy) / twochic2;

		for(int k=1; k < nvelocities; k++)
		{
			edotu = cspeed_x[k] * ux + cspeed_y[k] * uy;

			dgf_two[(k+kfeq)*lnodes+lpos] = (dgf_two[kfeq*lnodes+lpos] + edotu / chic2 + edotu * edotu / twochi2c4) *
														      nn  * cweight[k];

		}

		dgf_two[kfeq*lnodes+lpos] *= (nn  * cweight[0]);

		//dtimetau = delta_time / ntau;
/*
		dgf_one[0*nodes+gpos] = dgf_two[0*lnodes+lpos];
		dgf_one[1*nodes+gpos] = dgf_two[1*lnodes+lpos] - cfl * (dgf_two[1*lnodes+lpos] - dgf_two[upos1]);
		dgf_one[2*nodes+gpos] = dgf_two[2*lnodes+lpos] - cfl * (dgf_two[2*lnodes+lpos] - dgf_two[upos2]);
		dgf_one[3*nodes+gpos] = dgf_two[3*lnodes+lpos] - cfl * (dgf_two[3*lnodes+lpos] - dgf_two[upos3]);
		dgf_one[4*nodes+gpos] = dgf_two[4*lnodes+lpos] - cfl * (dgf_two[4*lnodes+lpos] - dgf_two[upos4]);
		dgf_one[5*nodes+gpos] = dgf_two[5*lnodes+lpos] - cfl * (dgf_two[5*lnodes+lpos] - dgf_two[upos5]);
		dgf_one[6*nodes+gpos] = dgf_two[6*lnodes+lpos] - cfl * (dgf_two[6*lnodes+lpos] - dgf_two[upos6]);
		dgf_one[7*nodes+gpos] = dgf_two[7*lnodes+lpos] - cfl * (dgf_two[7*lnodes+lpos] - dgf_two[upos7]);
		dgf_one[8*nodes+gpos] = dgf_two[8*lnodes+lpos] - cfl * (dgf_two[8*lnodes+lpos] - dgf_two[upos8]);
*/

		dgf_one[0*nodes+gpos] = dgf_two[0*lnodes+lpos];
		dgf_one[1*nodes+gpos] = dgf_two[1*lnodes+lpos] - cfl * (dgf_two[1*lnodes+lpos] - dgf_two[1 * lnodes + lpos - 1]);
		dgf_one[2*nodes+gpos] = dgf_two[2*lnodes+lpos] - cfl * (dgf_two[2*lnodes+lpos] - dgf_two[2 * lnodes + lpos - lnodes_x]);
		dgf_one[3*nodes+gpos] = dgf_two[3*lnodes+lpos] - cfl * (dgf_two[3*lnodes+lpos] - dgf_two[3 * lnodes + lpos + 1]);
		dgf_one[4*nodes+gpos] = dgf_two[4*lnodes+lpos] - cfl * (dgf_two[4*lnodes+lpos] - dgf_two[4 * lnodes + lpos + lnodes_x]);
		dgf_one[5*nodes+gpos] = dgf_two[5*lnodes+lpos] - cfl * (dgf_two[5*lnodes+lpos] - dgf_two[5 * lnodes + lpos - lnodes_x - 1]);
		dgf_one[6*nodes+gpos] = dgf_two[6*lnodes+lpos] - cfl * (dgf_two[6*lnodes+lpos] - dgf_two[6 * lnodes + lpos - lnodes_x + 1]);
		dgf_one[7*nodes+gpos] = dgf_two[7*lnodes+lpos] - cfl * (dgf_two[7*lnodes+lpos] - dgf_two[7 * lnodes + lpos + lnodes_x + 1]);
		dgf_one[8*nodes+gpos] = dgf_two[8*lnodes+lpos] - cfl * (dgf_two[8*lnodes+lpos] - dgf_two[8 * lnodes + lpos + lnodes_x - 1]);

		for(int k=0; k < nvelocities; k++)
		{

			//csux = cspeed_x[k] - ux;
			//csuy = cspeed_y[k] - uy;

			dgf_one[k*nodes+gpos] -= ((dgf_two[k*lnodes+lpos] - dgf_two[(k+kfeq)*lnodes+lpos]) * (delta_time / ntau /*dtimetau*/)
						- dgf_two[(k+kfeq)*lnodes+lpos] * dtimetemp * (dgf_two[kforcex*lnodes+lpos] * (cspeed_x[k] - ux/*csux*/)
						+ dgf_two[kforcey*lnodes+lpos] * (cspeed_y[k] - uy/*csuy*/)));
		}

	gpos += gridDim.x * blockDim.x;
	}

}


 void CUDAHandleError( cudaError_t err,
                         const char *file,
                         int line ) {

    if (err != cudaSuccess) {
        printf("%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define CUDA_HANDLE_ERROR( err ) (CUDAHandleError( err, __FILE__, __LINE__ ))


#define HANDLE_NULL( a ) {if (a == NULL) { \
                            printf( "Host memory failed in %s at line %d\n", \
                                    __FILE__, __LINE__ ); \
                            exit( EXIT_FAILURE );}}


/*-------------------- Host Global Constants ------------------*/
cudaError_t error;

const double hchi = (((double) 1) / ((double) 3));

const double hzero = ((double) 0), hone = ((double) 1), htwo = ((double) 2),
		hthree = ((double) 3), hfour = ((double) 4), hfive = ((double) 5),
		height = ((double) 8), hlength = ((double) 1);

int hnw              = 2;
int hnvar            = 27;
int hnbasevelocities = 2;
int hnvelocities     = 9;

int hkn              = hnvelocities;

int hkux             = hkn + 1;
int hkuy             = hkn + 2;

int hkpid            = hkn + 3;
int hkpwaals         = hkn + 4;
int hkdpress         = hkn + 5;
int hkforcex         = hkn + 6;
int hkforcey         = hkn + 7;
int hkfeq            = hkn + 8;

int rank;
int ix, iy, k, hiter, hicycle;

int hnodes_x, hnodes_y, hnodes, hlnodes_x, 
hlnodes_y, hlnodes;
int hniter, hniter_cycle, hncycles, hlast_iter;

double *hgf, *hfeq;

double  hcfl;
double *hbweight, *hcweight, *hbspeed;
double *hcspeed_x, *hcspeed_y;

double hbbspeed;
double hdelta_space, hdelta_time, htwo_delta_space, 
hdelta_space_three_three, hdtimetemp;
double htemperature, htfluid, htcritical, hRTzero, 
htau, hkappa, hrho_zero;
double hchic2, htwochic2, htwochi2c4; 

/*------------------ Device Constants ------------------*/


double *dgf_one, *dgf_two;
double *cspeed_x, *cspeed_y, *cweight;


int device_allocation()
{	
	CUDA_HANDLE_ERROR(cudaMalloc((void**)&dgf_one,hkfeq * hnodes * sizeof(double)));

	CUDA_HANDLE_ERROR(cudaMalloc((void**)&dgf_two,hnvar  * hlnodes * sizeof(double)));

	CUDA_HANDLE_ERROR(cudaMalloc((void**)&cspeed_x,hnvelocities * sizeof(double)));

	CUDA_HANDLE_ERROR(cudaMalloc((void**)&cspeed_y,hnvelocities * sizeof(double)));

	CUDA_HANDLE_ERROR(cudaMalloc((void**)&cweight,hnvelocities * sizeof(double)));

	return 0;
}

int host_allocation()
{
	hgf       = (double*)calloc(hkfeq * hnodes, sizeof(double));
	CHK_NULL_RETURN(hgf);

	hfeq      = (double*)calloc(hnvelocities, sizeof(double));
	CHK_NULL_RETURN(hfeq);

	hbweight  = (double*)calloc(hnbasevelocities, sizeof(double));
	CHK_NULL_RETURN(hbweight);

	hbspeed   = (double*)calloc(hnbasevelocities, sizeof(double));
	CHK_NULL_RETURN(hbspeed);

	hcweight  = (double*)calloc(hnvelocities, sizeof(double));
	CHK_NULL_RETURN(hcweight);

	hcspeed_x = (double*)calloc(hnvelocities, sizeof(double));
	CHK_NULL_RETURN(hcspeed_x);

	hcspeed_y = (double*)calloc(hnvelocities, sizeof(double));
	CHK_NULL_RETURN(hcspeed_y);

	return SUCCESS;

}

int copy_constants_to_device()
{
	cudaMemcpyToSymbol(zero,&hzero,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(one,&hone,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(two,&htwo,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(three,&hthree,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(four,&hfour,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(five,&hfive,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(eight,&height,sizeof(double),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(nodes_x,&hnodes_x,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(nodes_y,&hnodes_y,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(nodes,&hnodes,sizeof(int),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(lnodes_x,&hlnodes_x,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(lnodes_y,&hlnodes_y,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(lnodes,&hlnodes,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(nw,&hnw,sizeof(int),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(nvar,&hnvar,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(nbasevelocities,&hnbasevelocities,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(nvelocities,&hnvelocities,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kn,&hkn,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kux,&hkux,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kuy,&hkuy,sizeof(int),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(kpid,&hkpid,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kpwaals,&hkpwaals,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kdpress,&hkdpress,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kforcex,&hkforcex,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kforcey,&hkforcey,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kfeq,&hkfeq,sizeof(int),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(niter,&hniter,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(niter_cycle,&hniter_cycle,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(ncycles,&hncycles,sizeof(int),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(last_iter,&hlast_iter,sizeof(int),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(delta_time,&hdelta_time,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(dtimetemp,&hdtimetemp,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(delta_space,&hdelta_space,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(two_delta_space,&htwo_delta_space,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(delta_space_three_three,&hdelta_space_three_three,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(cfl,&hcfl,sizeof(double),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(chic2,&hchic2,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(twochic2,&htwochic2,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(twochi2c4,&htwochi2c4,sizeof(double),0,
			cudaMemcpyHostToDevice);

	cudaMemcpyToSymbol(tau,&htau,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(kappa,&hkappa,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(tfluid,&htfluid,sizeof(double),0,
			cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(tcritical,&htcritical,sizeof(double),0,
			cudaMemcpyHostToDevice);

	return cudaSuccess;
}

int copy_to_device()
{

	CUDA_HANDLE_ERROR(cudaMemcpy(dgf_one, hgf,hkfeq * hnodes * sizeof(double),cudaMemcpyHostToDevice));


	CUDA_HANDLE_ERROR(cudaMemcpy(cspeed_x, hcspeed_x,hnvelocities * sizeof(double),cudaMemcpyHostToDevice));


	CUDA_HANDLE_ERROR(cudaMemcpy(cspeed_y, hcspeed_y,hnvelocities  * sizeof(double),cudaMemcpyHostToDevice));


	CUDA_HANDLE_ERROR(cudaMemcpy(cweight, hcweight,hnvelocities * sizeof(double),cudaMemcpyHostToDevice));

	return SUCCESS;
}

int copy_to_host()
{

	CUDA_HANDLE_ERROR(cudaMemcpy(hgf, dgf_one,hkfeq * hnodes *sizeof(double),cudaMemcpyDeviceToHost));

	return SUCCESS;
}


int device_free()
{	

	CUDA_HANDLE_ERROR(cudaFree(dgf_one));

	CUDA_HANDLE_ERROR(cudaFree(dgf_two));

	CUDA_HANDLE_ERROR(cudaFree(cspeed_x));

	CUDA_HANDLE_ERROR(cudaFree(cspeed_y));

	CUDA_HANDLE_ERROR(cudaFree(cweight));

	return SUCCESS;
}

int host_free()
{
	free(hgf);
	free(hfeq);
	free(hbweight);
	free(hbspeed);
	free(hcweight);
	free(hcspeed_x);
	free(hcspeed_y);

	return SUCCESS;
}


int main(void)
{
	int wpid; 
	time_t now = time(NULL);
	struct tm* tm = localtime(&now);

	//cudaEvent_t start, stop; 
	float  time;
	int devID = 0;

	wpid  = getpid();

	cudaDeviceProp deviceProp;
	CUDA_HANDLE_ERROR(cudaGetDevice(&devID));
	CUDA_HANDLE_ERROR(cudaGetDeviceProperties(&deviceProp, devID));
	printf("\nGPU Device %d: \"%s\" with compute capability %d.%d\n\n",
			devID, deviceProp.name, deviceProp.major, deviceProp.minor);


	printf("Start date = %d/%d/%d  Start time = %d:%d:%d\n\n",tm->tm_mday,tm->tm_mon + 1,
			1900 + tm->tm_year, tm->tm_hour, tm->tm_min, tm->tm_sec);
	printf("Process %d \n\n\n",wpid);  

	readInputParameters("nw2cuda.input");
	lb_input(&hnodes, &hnodes_x, &hnodes_y, &hniter, &hniter_cycle, &hncycles, &hlast_iter,
			&hlnodes, &hlnodes_x, &hlnodes_y,&hbbspeed, &htcritical, &htfluid, &hchic2,
			&htwochic2, &htwochi2c4, &hdtimetemp,&hRTzero, &htemperature, &hdelta_time,
			&hkappa, &hrho_zero,&htau, &hdelta_space, &htwo_delta_space, &hdelta_space_three_three,
			hnw, hlength, hone, htwo, hthree, hchi);

	dim3 blocks((hnodes + 511)/512,1,1);		//linear
	dim3 threads(512, 1 ,1);	//linear
//	dim3 blocks(1,512,1);
//	dim3 threads(512, 1, 1);

	//cudaEventCreate(&start);
	//cudaEventCreate(&stop);

	host_allocation();
	device_allocation();

	init_base_vectors(hbweight, hcweight, hbspeed, hcspeed_x, hcspeed_y, &hcfl,
			hbbspeed, hzero, hdelta_time, hdelta_space);

	init_arrays(hgf, hfeq, hcspeed_x, hcspeed_y, hcweight, hnodes, hnodes_x, hnodes_y,
			hnvelocities, hkn, hzero, hone, hrho_zero, hchic2, htwochic2, htwochi2c4);

	hniter = hlast_iter;


	char name[128];
	build_names(name, hnodes_x, hnodes_y,
			hdelta_space, hdelta_time, htau, hrho_zero, hRTzero, htfluid, hkappa);
	profile(name, hgf, hnodes, hnodes_x, hnodes_y,hkn, hniter, hlength, hzero, htwo, hdelta_space);

	copy_constants_to_device();
	copy_to_device();

	//cudaEventRecord(start,0);

	hicycle = 0;
	while(hicycle < hncycles)
	{
		hiter = 0;
		while(hiter < hniter_cycle)
		{
			//periodic_boundary<<<blocks, threads>>>(dgf_one, dgf_two, cspeed_x, cspeed_y);
	cudaEvent_t start, stop; 
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,0);
			periodic_boundary<<<blocks, threads>>>(dgf_one, dgf_two, cspeed_x, cspeed_y);
	cudaEventRecord(stop,0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	printf("\nTotal Time = %f ms\n", time);
			
			
			lb_time_step<<<blocks, threads>>>(dgf_one, dgf_two, cspeed_x, cspeed_y, cweight);

			hiter++;
			hniter++;
		}
		//cudaDeviceSynchronize();
		copy_to_host();
		profile(name, hgf, hnodes, hnodes_x, hnodes_y,hkn, hniter, hlength, hzero, htwo, hdelta_space);
		hicycle++;
	}

	//cudaEventRecord(stop,0);
	//cudaEventSynchronize(stop);
	//cudaEventElapsedTime(&time, start, stop);
	//printf("\nTotal Time = %f ms\n", time);


	//cudaEventDestroy(start);
	//cudaEventDestroy(stop);

	device_free();
	host_free();

	return 0;
}
